#include "get_next_line.h"


#include <stdio.h>	//for printf
#include <unistd.h>	//for write
#include <string.h> //for strlen
#include <fcntl.h>	//for open

int		main(int ac, char **av)
{
	int		*fd;
	int		i = 0;
	int		j = 0;
	int		ret;
	char	*line = NULL;

	fd = (int *)malloc(sizeof(int) * ac);
	while (++i <= ac - 1)
		fd[i - 1] = open(av[i], O_RDONLY);
	i = 0;
	while (j < ac - 1)
	{
		if ((ret = get_next_line(fd[i], &line)) > 0)
		{
			printf(" [fd%i-%i] %s\n", fd[i], ret, line);
			free(line);
			j = 0;
		}
		else if (ret == -1)
		{
			printf("[fd%i-%i] *ERROR*\n", fd[i], ret);
			free(line);
			j++;
		}
		else if (*line != '\0')
			printf(" [fd%i-%i] %s\n", fd[i], ret, line);
		else
		{
			printf("[fd%i-%i] %s *FINI*\n", fd[i], ret, line);
			free(line);
			j++;
		}
		i++;
		if (i >= ac - 1)
			i = 0;
	}
	free(fd);
	//while (1);
	return (0);
}

