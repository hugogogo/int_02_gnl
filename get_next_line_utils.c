/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: hulamy <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/11 00:26:54 by hulamy            #+#    #+#             */
/*   Updated: 2019/12/29 18:07:58 by hulamy           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*ft_strdup(const char *src)
{
	int		i;
	char	*str;

	i = 0;
	while (src[i] != '\0')
		i++;
	if (!(str = (char*)malloc(sizeof(*str) * (i + 1))))
		return (NULL);
	while (i-- >= 0)
		str[i + 1] = src[i + 1];
	return (str);
}

char	*ft_strchr(const char *s, int c)
{
	int	i;
	int	j;

	i = 0;
	j = -1;
	while (s[i])
		i++;
	while (++j < i + 1)
		if (s[j] == c)
			return ((char *)s + j);
	return (NULL);
}

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t	i;
	char	*cpsrc;
	char	*cpdst;

	i = -1;
	cpsrc = (char *)src;
	cpdst = (char *)dst;
	if (dst == src)
		return (dst);
	if (cpsrc < cpdst)
		while (len--)
			cpdst[len] = cpsrc[len];
	else
		while (++i < len)
			cpdst[i] = cpsrc[i];
	return (dst);
}

size_t	ft_strlen(const char *str)
{
	size_t i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char	*ft_strjoinfree(char const *s1, char const *s2)
{
	char	*str;
	int		len;
	int		j;

	if (!s1 || !s2)
		return (NULL);
	len = ft_strlen(s1) + ft_strlen(s2);
	if (!(str = (char *)malloc(sizeof(char) * (len + 1))))
		return (NULL);
	len = 0;
	j = 0;
	while (s1[j] != '\0')
		str[len++] = s1[j++];
	j = 0;
	while (s2[j] != '\0')
		str[len++] = s2[j++];
	str[len] = '\0';
	free((char*)s1);
	return (str);
}
